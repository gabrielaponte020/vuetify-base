import { EventEmitter } from 'events';
import isEmpty from 'lodash/isEmpty';
import api from '../api/auth';
import { gapi, googleApikey, googleClientId, googleScopes } from '../api/index';

const store = new EventEmitter();
const localUser = 'user';

export default store;

store.state = {
  isAuth: false,
  isGoogleAuth: false,
  isChecked: true,
  isLogout: false,
  user: {},
};

store.getLocalUser = () => {
  const user = localStorage.getItem(localUser);
  return (user ? JSON.parse(user) : null);
};

store.actions = {
  signin: ({ commit }, user) => api.signin(user),
  forgotPassword: ({ commit }, email) => api.forgotPassword(email),
  forgotPasswordReset: ({ commit }, email) => api.forgotPasswordReset(email),
  changePassword: ({ commit }, email) => api.changePassword(email),
  signinGoogle: ({ commit }, email) => api.signinGoogle(email),
  setCurrentUser: ({ commit }, user) => {
    commit('SET_CURRENT_USER', user);
    if (user) commit('SET_AUTHORIZATION_TOKEN', user.token);
  },
  getLocalUser: ({ commit }) => {
    const user = localStorage.getItem(localUser);
    localStorage.setItem(localUser, JSON.stringify(user));
    commit('SET_CURRENT_USER', user);
    commit('SET_AUTHORIZATION_TOKEN', user.token);
    return user;
  },
  logout: ({ commit }) => new Promise((resolve) => {
    commit('LOGOUT');
    commit('SET_AUTHORIZATION_TOKEN', null);
    resolve();
  }),
  getGoogleAuth: ({ commit }, immediate) => new Promise((resolve, reject) => {
    gapi.load('client', () => {
      gapi.client.setApiKey(googleApikey);
      gapi.auth.authorize({
        client_id: googleClientId,
        scope: googleScopes,
        immediate,
      }, (res) => {
        if (res && !res.error) {
          gapi.client.load('gmail', 'v1', () => {
            gapi.client.load('plus', 'v1', () => {
              gapi.client.plus.people.get({
                userId: 'me',
              }).execute((plusRes) => {
                commit('SET_GOOGLE_AUTH', true);
                resolve(plusRes);
              });
            });
          });
        } else {
          commit('SET_GOOGLE_AUTH', false);
          reject(res.error);
        }
      });
    });
  }),
};

store.mutations = {
  SET_CURRENT_USER(state, user) {
    state.user = user;
    state.isAuth = !isEmpty(user);
    state.isChecked = true;
    state.isLogout = false;
    if (user) localStorage.setItem(localUser, JSON.stringify(user));
  },
  SET_GOOGLE_AUTH(state, val) {
    state.isGoogleAuth = val;
  },
  GET_LOCAL_USER(state, user) {
    state.user = user;
    state.isAuth = !isEmpty(user);
  },
  LOGOUT: (state) => {
    state.user = {};
    state.isAuth = false;
    state.isLogout = true;
    state.isGoogleAuth = false;
    localStorage.removeItem(localUser);
  },
};
