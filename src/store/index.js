import Vuex from 'vuex';
import isEmpty from 'lodash/isEmpty';
import axios from 'axios';

const files = require.context('.', false, /\.js$/);
const modules = {};
files.keys().forEach((key) => {
  if (key === './index.js') return;
  modules[key.replace(/(\.\/|\.js)/g, '')] = files(key).default;
});

const store = new Vuex.Store({
  state: {
    isLoading: false,
    isError: false,
    isMessage: false,
    errors: {},
    message: {},
    changePass: false,
    modalTicket: false,
    windowWidth: 0,
    windowHeight: 0,
    less600: false,
    less768: false,
    less1124: false,
  },
  modules,
  actions: {
    setLoading: ({ commit }, v) => {
      commit('LOADING', v);
    },
    setError: ({ commit }, v) => {
      commit('ERROR', v);
    },
  },

  mutations: {
    LOADING(state, val) {
      state.isLoading = val;
    },
    ERROR(state, err) {
      state.errors = err;
      state.isError = !isEmpty(state.errors);
    },
    SET_MESSAGE(state, msg) {
      state.message = { msg: msg.msg, type: msg.type, params: msg.params };
      state.isMessage = !isEmpty(state.message.msg);
    },
    SET_AUTHORIZATION_TOKEN(state, token) {
      if (!isEmpty(token)) axios.defaults.headers.common.Authorization = `Bearer ${token}`;
      else delete axios.defaults.headers.common.Authorization;
    },
    WINDOW_SIZE(state, size) {
      const width = size.width;
      const height = size.height;
      state.windowWidth = width;
      state.windowHeight = height;
      state.less600 = (width < 768);
      state.less768 = (width === 768 && width < 1124);
      state.less1124 = (width >= 1124);
    },
    REFRESH_AUTHORIZATION_TOKEN(state, token) {
      if (!isEmpty(token)) axios.defaults.headers.common.Authorization = token;
      else delete axios.defaults.headers.common.Authorization;
    },
  },
});

export default store;
