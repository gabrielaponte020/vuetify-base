import Vue from 'vue';
import { sync } from 'vuex-router-sync';
import App from './App';
import './plugins';
import router from './router';
import store from './store/index';
import './assets/scss/site.scss';

Vue.config.productionTip = false;
sync(store, router);

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
});
