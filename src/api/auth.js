import axios from 'axios';
import { apiUrl } from './';

export default {
  async signin(params) {
    return axios.post(`${apiUrl}/auth/login`, params);
  },
  async signinGoogle(params) {
    return axios.post(`${apiUrl}/auth/login/google`, params);
  },
  async signout(params) {
    return axios.post(`${apiUrl}/auth/logout`, params);
  },
  async forgotPassword(params) {
    return axios.post(`${apiUrl}/auth/password/reset`, params);
  },
  async forgotPasswordReset(params) {
    return axios.post(`${apiUrl}/auth/forgotPasswordReset/`, params);
  },
  async changePassword(params) {
    return axios.post(`${apiUrl}/auth/changePassword`, params);
  },
};
