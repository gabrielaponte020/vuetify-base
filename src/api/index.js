// Default ApiRestClient
export const apiUrl = process.env.API_URL;

// Default Path
export const baseUrl = process.env.BASE_URL;

// Google Api
export const gapi = window.gapi;
export const googleApikey = 'AIzaSyDFqfE8EVBBedsxxvMb8Dq4di5sJiSeMQA';
export const googleClientId = '533372092903-hvqt7ho07i7s7kbctr9g83f6rioudrvv.apps.googleusercontent.com';
export const googleScopes = [
  'https://www.googleapis.com/auth/gmail.readonly',
  'https://www.googleapis.com/auth/gmail.send',
  'https://www.googleapis.com/auth/userinfo.profile',
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/drive',
  'profile',
];


export const xmlHttp = (type, url) => new Promise((resolve) => {
  const xmlHttpr = new XMLHttpRequest();
  xmlHttpr.open(type, url, true);
  xmlHttpr.onload = () => {
    if (xmlHttpr.readyState === 4) {
      if (xmlHttpr.status === 200) {
        resolve(xmlHttpr.responseText);
      } else {
        // console.error(xmlHttpr.statusText);
      }
    }
  };
  xmlHttpr.send(null);
});

export const xmlHttpDownloadFile = (type, url, headers, token) => new Promise((resolve) => {
  const xmlHttpr = new XMLHttpRequest();
  xmlHttpr.open(type, url, true);
  xmlHttpr.responseType = 'blob';
  if (headers) xmlHttpr.setRequestHeader('Authorization', `Bearer ${token}`);
  xmlHttpr.onreadystatechange = () => {
    if (xmlHttpr.readyState === 4) {
      resolve(xmlHttpr.response);
    }
  };
  xmlHttpr.send(null);
});

export const fullPath = process.env.FULL_PATH;
