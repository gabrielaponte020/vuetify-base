import LayoutEmpty from '../layouts/LayoutEmpty';

export default [{
  path: '/auth',
  component: LayoutEmpty,
  children: [
    {
      path: 'signin',
      name: 'signin',
      component: () => import(/* webpackChunkname: "signin" */'../pages/auth/Signin'),
    },
  ],
}];
