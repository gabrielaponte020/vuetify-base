import Layout from '../layouts/Layout';
import { requireAuth } from '../plugins/util';

export default [{
  path: '/organization',
  component: Layout,
  children: [
    {
      path: 'chart',
      name: 'organization-chart',
      component: () => import(/* webpackChunkname: "organization-chart" */'../pages/organization/OrganizationChart'),
      beforeEnter: requireAuth,
    },
  ],
}];
