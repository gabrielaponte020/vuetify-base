import Router from 'vue-router';

const files = require.context('.', false, /\.js$/);
const modules = [];
files.keys().forEach((key) => {
  if (key === './index.js') return;
  files(key).default.map(r => modules.push(r));
});

const router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: modules,
});

export default router;
