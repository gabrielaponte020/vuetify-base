import Vue from 'vue';
import * as VueGoogleMaps from 'vue2-google-maps';

Vue.use(VueGoogleMaps, {
  load: {
    key: 'AIzaSyAqkVFUzavI82reg2kW5OVsv7yJjLNiVXI',
    libraries: 'places',
  },
});
