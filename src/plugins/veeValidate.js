import Vue from 'vue';
import VeeValidate from 'vee-validate';

const config = {
  errorBagName: 'formErrors', // change if property conflicts.
  fieldsBagName: 'formFields',
  events: 'input|blur',
  inject: true,
  strict: false,
};

Vue.use(VeeValidate, config);
