import LoadingComponent from '@/components/shared/LoadingComponent';
import auth from '../store/auth';

export const rules = {
  isEmpty: [v => !!v],
};

export function requireAuth(to, from, next) {
  if (!auth.getLocalUser()) {
    next({
      path: '/auth/signin',
      query: { redirect: to.fullPath },
    });
  } else {
    next();
  }
}

export function asyncComponent(componentName) {
  return import(`@/components/${componentName}/${componentName}.vue`);
}

export function asyncRoute(routeName) {
  return {
    path: `/${routeName}`,
    name: routeName,
    component: () => import(/* webpackChunkName: "lazy-route-chunk-[request]" */ `@/pages/${routeName}`),
  };
}

export function asyncLoading() {
  const FancyAsyncComponent = componentPromise => ({
    // The component to load. Should be a Promise
    component: componentPromise,
    // A component to use while the async component is loading
    loading: LoadingComponent,
    // A component to use if the load fails
    error: LoadingComponent,
    // Delay before showing the loading component. Default: 200ms.
    delay: 200,
    // The error component will be displayed if a timeout is
    // provided and exceeded. Default: Infinity.
    timeout: 3000,
  });

  return FancyAsyncComponent;
}
