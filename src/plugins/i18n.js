import Vue from 'vue';
import VueI18n from 'vue-i18n';
import en from './en-US.json';
import es from './es-ES.json';

Vue.use(VueI18n);

export default new VueI18n({
  locale: 'es',
  messages: {
    en,
    es,
  },
});
