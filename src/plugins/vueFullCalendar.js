import Vue from 'vue';
import vueFullCalendar from 'vue-full-calendar';

Vue.use(vueFullCalendar);
